# Cours Design Ethique

## Objectifs pédagogiques
- Mener une analyse réflexive
- Développer un point de vue critique sur son environnement
- Comprendre les enjeux d'éthique et de responsabilité dans les entreprises
- Ouvrir son spectre d'analyse d'une situation
- Assimiler les notions d'éthique, d'externalités et de objectifs ≠ valeurs

## Lectures conseillées
- [Zebras Fix What Unicorns Break - Jennifer, Mara, Astrid & Aniyia - Medium](https://medium.com/@sexandstartups/zebrasfix-c467e55f9d96)
- [BBC - Ethics: Introduction to ethics](http://www.bbc.co.uk/ethics/introduction/)
- [Qu'est-ce que le design ? Projet, industrie, société](http://www.stephane-vial.net/quest-ce-que-le-design/)
- [Peut-on rendre la technologie morale ? - Mais où va le Web](http://maisouvaleweb.fr/peut-on-rendre-la-technologie-morale/)
- [Retour sur l'atelier "services numériques, quels enjeux éthiques ? Comment les repenser ?" - Mais où va le Web](http://maisouvaleweb.fr/retour-latelier-services-numeriques-enjeux-ethiques-repenser/)
- [De l'importance d'être luddite (et autres réflexions sur les discours technocritiques) - Mais où va le Web](http://maisouvaleweb.fr/luddisme-technocritique/)

## Déroulé du cours - 6 heures

### Introductions - 1 heure

#### Présentations - 15 minutes
- Vous et vos entreprises + missions
- Moi (Designers Éthiques et doctorat)
    - [Designers Éthiques - Association loi 1901 promouvant la conception de services respectueux des usagers](https://designersethiques.org)
- Ethics by design 2018
    - [Les Designers Ethiques](https://vimeo.com/designersethiques)
    - [Ethics by design 2018](https://2018.ethicsbydesign.fr)

#### Qu'est-ce que l'éthique ? - 45 minutes
- Théorie (approche design)
    - [Qu'est-ce que le design ? Projet, industrie, société](http://www.stephane-vial.net/quest-ce-que-le-design/)
    - [Ethics for design par Gauthier Roussilhe](https://vimeo.com/232973887)
- L'éthique pour vous, comment la définir
    - [Moral Machine](http://moralmachine.mit.edu/)
    - [De la relativité de l'éthique](http://www.internetactu.net/a-lire-ailleurs/de-la-relativite-de-lethique/)
- L'éthique pour une entreprise, comment est-elle définie (ex de Jones)
    - [Ethics - Our Mission | Jones Snowboards](https://www.jonessnowboards.com/de-CH/ethics/our-mission.html)
- La question de la responsabilité
    - [La responsabilité sociétale des entreprises](https://www.ecologique-solidaire.gouv.fr/responsabilite-societale-des-entreprises)
    - [Quel poids pèsent vraiment les gestes individuels dans le changement climatique?](http://www.slate.fr/story/167444/changement-climatique-responsabilite-individus-entreprises)

### L'éthique dans votre activité - 55 minutes
Cet exercice est tiré d'une méthode sprint développée par Maheen Sohail, accessible ici : https://drive.google.com/file/d/0B_BduP_zkbNWZWVRSkpxNGtTRG8/view

#### Les problèmes déjà rencontrés - 15 minutes
- Les problèmes aux réponses évidentes et ceux aux réponses plus subtiles
- Les tensions existantes
- La prévision des externalités et l'évaluation des idées

#### Objectif - 15 minutes
- Quel est le but de votre entreprise / votre but ?
- Quelles valeurs correspondent à ce but ?

### Evaluer l'existant - 30 minutes
[Vers l'exercice](https://www.ethicsfordesigners.com/description/)

> Describe the ‘WHAT’ of the design. Use the questions on the template.

> Describe the ‘HOW’ of the design. Use this to determine the script: Like the script of a movie or a theatre play, an artefact can ‘prescribe’ its users how to act when they use it. Write this down.

> Describe the ‘WHY’ of the design. Use this to determine the underlying worldview.  Write this down.

### Prévoir / prédire - 30 minutes
- [The Tarot Cards Of Tech](http://tarotcardsoftech.artefactgroup.com/)
- Compléter l'un des documents suivants :
    - Arbre d'impacts
    - [Dichotomy Mapping](https://www.designethically.com/dichotomy-mapping)

### Sensibiliser les designers - 90 minutes
**Créer une grille d'analyse simple et réflexive de son entreprise et ses missions (activités) au travers des questions et implications éthiques**

- Contrainte de forme : une feuille A4 RV ou une feuille A3 R (outil de création libre)
- Objectif : Un outil simple d'analyse, qui ne prend pas beaucoup de temps à remplir
- Questions à se poser
    - quand la remplir ?
    - qui peut ou doit la remplir ?
    - doit-on avoir un résultat à la fin ?
    - que se passe-t-il après la grille d'analyse (solutions, changements)
- Idées pour la forme
    - Formulaire
    - Test / quiz
    - Cases à remplir
- Liens d'inspiration
    - Design éthique
        - [Ind.ie - Ethical Design Manifesto](https://2017.ind.ie/ethical-design/)
        - [Designing ethically pt. 1 - UX Collective](https://uxdesign.cc/designing-ethically-pt-1-9800bfbc86a3)
        - [Designing ethically pt. 2 - UX Collective](https://uxdesign.cc/designing-ethically-pt-2-535ac61e2992)
    - Outils d'auto-évaluation
        - [Guide d'auto-évaluation de l'utilité sociale à l'intention des acteurs de l'ESS | Avise.org](https://www.avise.org/ressources/guide-dauto-evaluation-de-lutilite-sociale-a-lintention-des-acteurs-de-less)
    - Évaluation de la qualité
        - [Les outils d'évaluation](http://www.qualiteperformance.org/comprendre-la-qualite/methodes-et-outils-pour-pratiquer-la-qualite/les-outils-d-evaluation)

**Restitution - 20 minutes soit 5 minutes par groupe**

### Sensibiliser les autres - 90 minutes
**Créer un argumentaire pour rendre du pouvoir au designer, en partant du constat que le design est désormais pratiqué par beaucoup de personnes non-designers**

- Objectif : donner des "billes" (arguments, exemples) à des designers qui doivent défendre leur position en entreprise
- Attention : il s'agit de défendre le designer, pas le design
- Questions à se poser :
    - Qu'est-ce que le design ?
    - Quels liens avec la psychologie, la gestion de projet ?
    - Qu'apporte le designer par rapport aux autres professions ?
    - Qu'est-ce qu'un designer en contexte numérique ? (UI, UX)
- Idées pour la forme :
    - F.A.Q
    - Arbre logique
- Liens d'inspiration
    - [https://www.themakerygroup.com/designer-empowerment.html](Designer empowerment) 

[Créé par Jérémie Poiroux pour Designers Ethiques](https://sites.google.com/view/cours181119dsaavillefonaire/accueil)