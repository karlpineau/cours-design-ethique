# Liste de 72 “biais” ou troubles exploitables en marketing & design :
Source (non scientifique) : convertize.com 


## 1 L’effet Von Restorff
Un individu qui apprend une série d’éléments similaires (liste de mots, etc.) retiendra plus rapidement l’élément qui se distingue des autres.

## 2 Le principe de contraste
Lorsque l’on expérimente deux choses similaires de façon simultanée, notre perception de la seconde est influencée par celle de la première.

## 3 Le paradoxe du choix
Une abondance d’informations conduit un individu à prendre des décisions moins efficaces et moins satisfaisantes que celles qu’il aurait prises avec moins d’informations.

## 4 L’effet pom-pom girl
Nous trouvons les choses plus attractives quand elles sont présentées au sein d’un groupe.

## 5 L’aversion à la perte
Le principe d’aversion à la perte démontre que les individus sont plus sensibles aux perspectives de pertes qu’à celles associées aux gains.

## 6 L’effet de rareté
L’effet de rareté est un biais cognitif qui conduit un individu à donner davantage de valeur à un produit rare qu’à un produit disponible en abondance.

## 7 L’effet d’immédiateté
Lorsqu’un individu a le choix entre deux récompenses, il va privilégier la récompense qu’il peut recevoir le plus tôt (voire immédiatement). Ceci même si sa valeur est inférieure à celle qu’il pourrait obtenir plus tard.

## 8 Souffrance du paiement
Payer réduit le plaisir d’acheter, et cette « souffrance du paiement » dépend à la fois du mode de paiement et du délai entre la consommation et le paiement.

## 9 Principe de réciprocité
Le principe de réciprocité démontre l’efficacité de « donner pour avoir en retour ». Si on agit en faveur d’un individu, il sera plus enclin à agir en votre faveur également.

## 10 La peur de manquer
La peur de manquer est la peur de faire un mauvais choix qui nous ferait manquer une autre opportunité, potentiellement plus satisfaisante, qui se présentait à nous.

## 11 L’effet de leurre
Les individus décident s’ils veulent acheter un produit en le comparant aux autres options proposées. Ainsi, ajouter une troisième option lors de leur choix peut les influencer sur le produit qu’ils vont finalement choisir.

## 12 Effet du choix Hobson
Les individus sont plus susceptibles de se décider pour une option quand ils ont le choix entre deux, plutôt que lorsqu’il s’agit d’une option à prendre ou à laisser.

## 13 L’effet de position en série
L’effet de position en série décrit la prédisposition des individus à se souvenir davantage du premier et dernier éléments d’une liste, comparé aux autres éléments de la liste.

## 16 L’effet d’ambiguïté
L’effet d’ambiguïté insiste sur la préférence des individus pour les risques connus vis-à-vis des risques moins connus.

## 17 L’effet de l’esthétique-pratique
Un produit au design plus esthétique est perçu comme plus facile d’utilisation qu’un design moins esthétique. Il sera donc davantage utilisé que les autres, quelles que soient ses fonctionnalités.

## 18 L’effet d’ancrage
Dans un contexte donné, les individus ont tendance à utiliser la première information reçue comme point d’ancrage. Ils s’en servent donc pour juger des autres informations qu’ils reçoivent par la suite.

## 19 L’influence sociale 
Le principe de la preuve sociale est un biais cognitif de conformité. Il postule que lorsqu’une personne n’est pas certaine de la façon dont elle doit agir, elle va se fier aux autres pour déterminer le bon comportement à adopter

## 22 Dissonance cognitive
Nous avons tendance à vouloir garder une certaine harmonie entre toutes nos attitudes, habitudes et préférences. Par conséquent, nous cherchons perpétuellement à éviter toute dissonance.

## 23 L’effet Zeignarnik
Les objectifs que nous n’avons pas encore finaliser restent davantage ancrés dans notre mémoire que ceux qui ont été menés à terme.

## 24 L’aversion pour l’option unique
Lorsque l’on propose à des clients une seule option, ils ont tendance à chercher des options alternatives. Ils reportent donc leur achat.

## 28 Biais de soutien du choix
Lorsque l’on se remémore nos décisions passées, nous déformons nos souvenirs de sorte que les choix que nous avons faits semblent avoir été les meilleurs possibles.

## 29 La technique du pied dans la porte
La technique du pied dans la porte consiste à obtenir un avantage restreint avant de demander une véritable faveur.

## 30 L’effet d’exposition
Il s’agit du sentiment positif ressenti à la vue d’un élément familier.

## 31 L’effet de cadrage
C’est le biais cognitif par lequel nous réagissons différemment aux messages ou aux choix que l’on nous soumet en fonction de la manière dont on nous les présente.

## 32 L’intention et l’autorégulation
Formuler une intention précise sur la façon de réaliser un objectif peut doubler voire tripler vos chances de l’atteindre.

## 33 Malédiction du savoir
Le principe de malédiction du savoir est un biais cognitif qui rend difficile pour quelqu’un qui connaît bien un sujet de se mettre à la place de quelqu’un qui ne le connaît pas.

## 34 Le biais d’autonomie
Nous préférons les situations sur lesquelles nous avons le sentiment d’exercer un contrôle.

## 35 L’effet de focus
Lorsque nous décidons d’une action, nous devons limiter le nombre de facteurs que nous prenons en compte (car il y en aurait trop à intégrer). Nous donnons donc trop d’importance à certains facteurs, de manière arbitraire, ce qui biaise notre choix.

## 36 L’effet de dotation
L’effet de dotation conduit un individu à attribuer une valeur excessive à ce qu’il possède déjà.

## 37 L’effet d’avoir vs l’effet d’utiliser
La tendance des individus à préférer et à payer plus cher pour des produits contenant de multiples fonctionnalités, qu’importe leur utilité, par surévaluation de l’utilisation qu’ils vont en avoir.  

## 40 Le principe de repère visuel
Notre cerveau forme la plupart des images que l’on « voit », et celui-ci aime qu’on l’aide à se focaliser sur quelque chose en le dirigeant par des repères visuels tels que des flèches. Ces repères orientent notre attention sur certains éléments.

## 41 L’effet de la position du milieu
Face à une gamme de produits alignés côte à côte, nous avons tendance à préférer celui positionné au milieu.

## 42 Principe d’autorité
Notre tendance à obéir aux figures d’autorités, même lorsque la légitimité de ce qu’on nous demande peut être questionnée.

## 43 L’effet de saillance
Notre attention est attirée par ce qui est le plus judicieux pour nous au moment présent, et nous intégrons et retenons mieux les éléments logiques.

## 44 Le principe d’engagement et de cohérence
Une fois que nous nous sommes engagés publiquement à quelque chose ou envers quelqu’un, nous avons tendance à nous y tenir pour être perçu comme cohérent.

## 45 Représentativité heuristique
Les individus ont tendance à juger de la probabilité d’un évènement en se fiant à la récurrence d’évènements similaires ou plus généralement, à faire des prédictions et généralisations biaisées à partir de modèles similaires.

## 46 Processus d’encodage de l’ampleur
La façon dont nous percevons les prix peut être influencée par la façon dont on nous les présente (taille, disposition, couleur) afin que ceux-ci paraissent plus petit.

## 47 L’effet de supériorité de l’image
Les images ont un impact direct et différé supérieur aux mots dans notre mémoire.

## 48 L’effet de métaphore
Nous avons tendance à comprendre plus facilement et à nous souvenir davantage du langage métaphorique. Ce dernier fait en effet appel à notre imagerie mentale.

## 49 La réactance psychologique
Lorsque nous ressentons que quelqu’un ou quelque chose limite notre liberté d’action ou de décision, nous enclenchons un mécanisme de défense psychologique nommé la réactance.

## 50 L’effet de représentation visuelle
Les individus sont plus susceptibles d’acheter un produit s’ils peuvent s’imaginer en train de l’utiliser. Le produit doit donc être présenté dans ce sens.

## 54 L’effet d’ancienneté
Nous avons tendance à retenir les informations que nous avons entendues le plus récemment, et à leur attribuer une importance disproportionnée.

## 55 Le biais d’attention
Nous portons davantage d’attention aux choses qui nous touchent émotionnellement.

## 59 Effet Benjamin Franklin
Le fait de faire une faveur à quelqu’un nous amène à l’apprécier davantage et à être plus enclin à lui en faire une seconde. L’effet inverse, vis-à-vis de nos « cibles », est également vrai.

## 60 L’effet de l’incertitude motivante
Un système de récompenses dans lequel on introduit un degré d’incertitude peut augmenter l’investissement des individus dans la réalisation de leur mission.

## 61 La valeur du prix perçu
La valeur du prix perçu explique comment le prix d’un produit peut ne pas être forcément basé sur sa valeur réelle mais sur la perception qu’en a un client.

## 62 L’effet du prix de référence
L’effet du prix de référence désigne notre manière d’estimer le juste prix d’un produit ou d’un service en le comparant à d’autres (ceux des concurrents, d’anciens produits similaires achetés, etc.)

## 63 Tarification fragmentée
La tarification fragmentée est le fait que nous ayons tendance à considérer un prix total comme moins cher si celui-ci nous est présenté comme divisé en plusieurs sous-totaux.

## 64 La théorie de la comparaison sociale
La théorie de la comparaison sociale démontre notre tendance à nous évaluer en nous comparant aux individus qui nous sont proches plutôt qu’en valeurs absolues.

## 65 La compensation du risque
Le principe de compensation du risque explique la tendance humaine à prendre des risques plus importants lorsque les bénéfices sont perçus comme plus sûrs.

## 67 Le biais intragroupe
Nous avons tendance à favoriser les membres de notre groupe vis-à-vis de ceux qui en sont exclus.

## 70 Le ratio d’attention
Le ratio d’attention montre que les individus ont tendance à suivre une action s’ils restent focalisés sur celle-ci et que leur attention n’est pas divisée entre différents éléments.
